﻿using System;
using System.Linq;
using System.Windows.Forms;
using MetroFramework.Controls;
using MetroFramework.Forms;

namespace 三星彩
{
    public partial class Main : MetroForm
    {
        public Main()
        {
            InitializeComponent();
        }

        private void filterButton_Click(object sender, EventArgs e)
        {
            listView1.Clear();
            var allCheckBox = Controls.OfType<MetroCheckBox>();
            FilterHelper.GetAllCheckBoxStatus(allCheckBox);
            var result = FilterHelper.Result();
            result.ForEach(n => listView1.Items.Add(n));
            metroLabel10.Text = $"{result.Count}注，金額：{result.Count * 25}";
        }

        private void copyResultButton_Click(object sender, EventArgs e)
        {
            if (listView1.Items.Count != 0)
            {
                var result = $"{metroLabel10.Text}{Environment.NewLine}{Environment.NewLine}";
                foreach (ListViewItem number in listView1.Items)
                {
                    result = string.Concat(result, $"{number.Text}{Environment.NewLine}");
                }
                Clipboard.SetText(result);
            }
        }
    }
}