﻿using MetroFramework.Controls;
using System.Collections.Generic;
using System.Linq;

namespace 三星彩
{
    public static class FilterHelper
    {
        /// <summary>
        /// _allCheckBox
        /// </summary>
        private static IEnumerable<MetroCheckBox> _allCheckBox;

        /// <summary>
        /// _allNumber
        /// </summary>
        private static IEnumerable<string> _allNumber;

        /// <summary>
        /// 000 ~ 999
        /// </summary>
        private static IEnumerable<string> AllNumber
        {
            get
            {
                if (_allNumber == null)
                {
                    _allNumber = Enumerable.Range(0, 1000).Select(n => n.ToString().PadLeft(3, '0'));
                }
                return _allNumber;
            }
        }

        public static List<string> Result()
        {
            var filterAllNumber = 百十個位數過濾().和值過濾()
               .大小比過濾().奇偶比過濾().質合比過濾()
               .大小單選排列過濾().奇偶單選排列過濾().質合單選排列過濾();

            var DuplicationList = 有對子(filterAllNumber);
            var NoDuplicationList = 無對子(filterAllNumber);
            var LeopardList = 豹子(filterAllNumber);

            //增加一個空 List，加入篩選
            var result = new List<string>();
            result.AddRange(DuplicationList);
            result.AddRange(NoDuplicationList);
            result.AddRange(LeopardList);

            //回傳最後結果
            return result.OrderBy(n => n).ToList();
        }

        public static void GetAllCheckBoxStatus(IEnumerable<MetroCheckBox> allCheckBox)
        {
            _allCheckBox = allCheckBox;
        }

        private static List<string> 豹子(this IEnumerable<string> filterAllNumber)
        {
            //豹子(都一樣) Check Box
            var isLeopard = _allCheckBox
                 .Where(checkBox => checkBox.Tag != null && checkBox.Tag.Equals("Leopard")).First().Checked;
            //豹子(都一樣)
            var LeopardList = new List<string>();
            if (isLeopard)
            {
                LeopardList = filterAllNumber.Where(n =>
                {
                    var @char = n.ToCharArray();
                    var count = @char.Distinct().Count();
                    return count.Equals(1) ? true : false;
                }).ToList();
            }

            return LeopardList;
        }

        private static List<string> 無對子(this IEnumerable<string> filterAllNumber)
        {
            //無對子(無重複) Check Box
            var isNoDuplication = _allCheckBox
                 .Where(checkBox => checkBox.Tag != null && checkBox.Tag.Equals("No Duplication")).First().Checked;
            //無對子(無重複)
            var NoDuplicationList = new List<string>();
            if (isNoDuplication)
            {
                NoDuplicationList = filterAllNumber.Where(n =>
                {
                    var @char = n.ToCharArray();
                    var count = @char.Distinct().Count();
                    return count.Equals(3) ? true : false;
                }).ToList();
            }

            return NoDuplicationList;
        }

        private static List<string> 有對子(this IEnumerable<string> filterAllNumber)
        {
            //有對子(有重複) Check Box
            var isDuplication = _allCheckBox
                 .Where(checkBox => checkBox.Tag != null && checkBox.Tag.Equals("Duplication")).First().Checked;
            //有對子(有重複)
            var DuplicationList = new List<string>();
            if (isDuplication)
            {
                DuplicationList = filterAllNumber.Where(n =>
                {
                    var @char = n.ToCharArray();
                    var count = @char.Distinct().Count();
                    return count.Equals(2) ? true : false;
                }).ToList();
            }

            return DuplicationList;
        }

        private static IEnumerable<string> 質合單選排列過濾(this IEnumerable<string> filterAllNumber)
        {
            //Group-3(質合單選排列) Check Box
            var group3 = _allCheckBox
                 .Where(checkBox => checkBox.Tag != null && checkBox.Tag.Equals("Group-3") && checkBox.Checked).Select(t => t.Text);
            //Group-3(質合單選排列)
            var primeNumberList = new List<char>
            {
                '1', '2' ,'3' ,'5' ,'7'
            };
            var group3List = new List<string>();
            if (group3.Any(s => s.Equals("質質質")))
            {
                group3List.AddRange(AllNumber.Where(n =>
                {
                    var @char = n.ToCharArray();
                    return (primeNumberList.Contains(@char[0]) && primeNumberList.Contains(@char[1]) && primeNumberList.Contains(@char[2])) ? true : false;
                })
                );
            }

            if (group3.Any(s => s.Equals("質質合")))
            {
                group3List.AddRange(AllNumber.Where(n =>
                {
                    var @char = n.ToCharArray();
                    return (primeNumberList.Contains(@char[0]) && primeNumberList.Contains(@char[1]) && !primeNumberList.Contains(@char[2])) ? true : false;
                })
                );
            }

            if (group3.Any(s => s.Equals("質合質")))
            {
                group3List.AddRange(AllNumber.Where(n =>
                {
                    var @char = n.ToCharArray();
                    return (primeNumberList.Contains(@char[0]) && !primeNumberList.Contains(@char[1]) && primeNumberList.Contains(@char[2])) ? true : false;
                })
                );
            }

            if (group3.Any(s => s.Equals("質合合")))
            {
                group3List.AddRange(AllNumber.Where(n =>
                {
                    var @char = n.ToCharArray();
                    return (primeNumberList.Contains(@char[0]) && !primeNumberList.Contains(@char[1]) && !primeNumberList.Contains(@char[2])) ? true : false;
                })
                );
            }

            if (group3.Any(s => s.Equals("合質質")))
            {
                group3List.AddRange(AllNumber.Where(n =>
                {
                    var @char = n.ToCharArray();
                    return (!primeNumberList.Contains(@char[0]) && primeNumberList.Contains(@char[1]) && primeNumberList.Contains(@char[2])) ? true : false;
                })
                );
            }

            if (group3.Any(s => s.Equals("合質合")))
            {
                group3List.AddRange(AllNumber.Where(n =>
                {
                    var @char = n.ToCharArray();
                    return (!primeNumberList.Contains(@char[0]) && primeNumberList.Contains(@char[1]) && !primeNumberList.Contains(@char[2])) ? true : false;
                })
                );
            }

            if (group3.Any(s => s.Equals("合合質")))
            {
                group3List.AddRange(AllNumber.Where(n =>
                {
                    var @char = n.ToCharArray();
                    return (!primeNumberList.Contains(@char[0]) && !primeNumberList.Contains(@char[1]) && primeNumberList.Contains(@char[2])) ? true : false;
                })
                 );
            }

            if (group3.Any(s => s.Equals("合合合")))
            {
                group3List.AddRange(AllNumber.Where(n =>
                {
                    var @char = n.ToCharArray();
                    return (!primeNumberList.Contains(@char[0]) && !primeNumberList.Contains(@char[1]) && !primeNumberList.Contains(@char[2])) ? true : false;
                })
                );
            }

            filterAllNumber = filterAllNumber.Intersect(group3List).Distinct();
            return filterAllNumber;
        }

        private static IEnumerable<string> 奇偶單選排列過濾(this IEnumerable<string> filterAllNumber)
        {
            //Group-2(奇偶單選排列) Check Box
            var group2 = _allCheckBox
                 .Where(checkBox => checkBox.Tag != null && checkBox.Tag.Equals("Group-2") && checkBox.Checked).Select(t => t.Text);
            //Group-2(奇偶單選排列)
            var oddNumberList = new List<char>
            {
                '1', '3' ,'5' ,'7' ,'9'
            };
            var group2List = new List<string>();
            if (group2.Any(s => s.Equals("奇奇奇")))
            {
                group2List.AddRange(AllNumber.Where(n =>
                {
                    var @char = n.ToCharArray();
                    return (oddNumberList.Contains(@char[0]) && oddNumberList.Contains(@char[1]) && oddNumberList.Contains(@char[2])) ? true : false;
                })
                );
            }

            if (group2.Any(s => s.Equals("奇奇偶")))
            {
                group2List.AddRange(AllNumber.Where(n =>
                {
                    var @char = n.ToCharArray();
                    return (oddNumberList.Contains(@char[0]) && oddNumberList.Contains(@char[1]) && !oddNumberList.Contains(@char[2])) ? true : false;
                })
               );
            }

            if (group2.Any(s => s.Equals("奇偶奇")))
            {
                group2List.AddRange(AllNumber.Where(n =>
                {
                    var @char = n.ToCharArray();
                    return (oddNumberList.Contains(@char[0]) && !oddNumberList.Contains(@char[1]) && oddNumberList.Contains(@char[2])) ? true : false;
                })
                );
            }

            if (group2.Any(s => s.Equals("奇偶偶")))
            {
                group2List.AddRange(AllNumber.Where(n =>
                {
                    var @char = n.ToCharArray();
                    return (oddNumberList.Contains(@char[0]) && !oddNumberList.Contains(@char[1]) && !oddNumberList.Contains(@char[2])) ? true : false;
                })
                );
            }

            if (group2.Any(s => s.Equals("偶奇奇")))
            {
                group2List.AddRange(AllNumber.Where(n =>
                {
                    var @char = n.ToCharArray();
                    return (!oddNumberList.Contains(@char[0]) && oddNumberList.Contains(@char[1]) && oddNumberList.Contains(@char[2])) ? true : false;
                })
                );
            }

            if (group2.Any(s => s.Equals("偶奇偶")))
            {
                group2List.AddRange(AllNumber.Where(n =>
                {
                    var @char = n.ToCharArray();
                    return (!oddNumberList.Contains(@char[0]) && oddNumberList.Contains(@char[1]) && !oddNumberList.Contains(@char[2])) ? true : false;
                })
                );
            }

            if (group2.Any(s => s.Equals("偶偶奇")))
            {
                group2List.AddRange(AllNumber.Where(n =>
                {
                    var @char = n.ToCharArray();
                    return (!oddNumberList.Contains(@char[0]) && !oddNumberList.Contains(@char[1]) && oddNumberList.Contains(@char[2])) ? true : false;
                })
                );
            }

            if (group2.Any(s => s.Equals("偶偶偶")))
            {
                group2List.AddRange(AllNumber.Where(n =>
                {
                    var @char = n.ToCharArray();
                    return (!oddNumberList.Contains(@char[0]) && !oddNumberList.Contains(@char[1]) && !oddNumberList.Contains(@char[2])) ? true : false;
                })
               );
            }

            filterAllNumber = filterAllNumber.Intersect(group2List).Distinct();
            return filterAllNumber;
        }

        private static IEnumerable<string> 大小單選排列過濾(this IEnumerable<string> filterAllNumber)
        {
            //Group-1(大小單選排列) Check Box
            var group1 = _allCheckBox
                 .Where(checkBox => checkBox.Tag != null && checkBox.Tag.Equals("Group-1") && checkBox.Checked).Select(t => t.Text);
            //Group-1(大小單選排列)
            var group1List = new List<string>();
            if (group1.Any(s => s.Equals("大大大")))
            {
                group1List.AddRange(Enumerable.Range(555, 1000).Select(n => n.ToString().PadLeft(3, '0')));
            }

            if (group1.Any(s => s.Equals("大大小")))
            {
                group1List.AddRange(Enumerable.Range(550, 994).Select(n => n.ToString().PadLeft(3, '0')));
            }

            if (group1.Any(s => s.Equals("大小大")))
            {
                group1List.AddRange(Enumerable.Range(505, 949).Select(n => n.ToString().PadLeft(3, '0')));
            }

            if (group1.Any(s => s.Equals("大小小")))
            {
                group1List.AddRange(Enumerable.Range(500, 944).Select(n => n.ToString().PadLeft(3, '0')));
            }

            if (group1.Any(s => s.Equals("小大大")))
            {
                group1List.AddRange(Enumerable.Range(055, 499).Select(n => n.ToString().PadLeft(3, '0')));
            }

            if (group1.Any(s => s.Equals("小大小")))
            {
                group1List.AddRange(Enumerable.Range(050, 494).Select(n => n.ToString().PadLeft(3, '0')));
            }

            if (group1.Any(s => s.Equals("小小大")))
            {
                group1List.AddRange(Enumerable.Range(005, 449).Select(n => n.ToString().PadLeft(3, '0')));
            }

            if (group1.Any(s => s.Equals("小小小")))
            {
                group1List.AddRange(Enumerable.Range(000, 444).Select(n => n.ToString().PadLeft(3, '0')));
            }

            filterAllNumber = filterAllNumber.Intersect(group1List).Distinct();
            return filterAllNumber;
        }

        private static IEnumerable<string> 質合比過濾(this IEnumerable<string> filterAllNumber)
        {
            //質合比 Check Box
            var primeCompositeRatio = _allCheckBox
                 .Where(checkBox => checkBox.Tag != null && checkBox.Tag.Equals("Prime Composite Ratio"))
                 .Where(checkBox => checkBox.Checked).Select(checkBox => int.Parse(checkBox.Text.Split(':')[0].Trim()));
            //質合比 過濾
            filterAllNumber = filterAllNumber.Where(n =>
            {
                var @char = n.ToCharArray().Select(c => c.ToString()).ToList();
                var primeNumber = new List<string> { "1", "2", "3", "5", "7" };
                var init = 0;
                @char.ForEach(v =>
                {
                    if (primeNumber.Contains(v))
                    {
                        init++;
                    }
                });

                return primeCompositeRatio.Contains(init) ? true : false;
            });
            return filterAllNumber;
        }

        private static IEnumerable<string> 奇偶比過濾(this IEnumerable<string> filterAllNumber)
        {
            //奇偶比 Check Box
            var parityRatio = _allCheckBox
                 .Where(checkBox => checkBox.Tag != null && checkBox.Tag.Equals("Parity Ratio"))
                 .Where(checkBox => checkBox.Checked).Select(checkBox => int.Parse(checkBox.Text.Split(':')[0].Trim()));
            //奇偶比 過濾
            filterAllNumber = filterAllNumber.Where(n =>
            {
                var @char = n.ToCharArray().Select(c => c.ToString()).ToList();
                var oddNumber = new List<string> { "1", "3", "5", "7", "9" };
                var init = 0;
                @char.ForEach(v =>
                {
                    if (oddNumber.Contains(v))
                    {
                        init++;
                    }
                });

                return parityRatio.Contains(init) ? true : false;
            });
            return filterAllNumber;
        }

        private static IEnumerable<string> 大小比過濾(this IEnumerable<string> filterAllNumber)
        {
            //大小比 Check Box
            var sizeRatio = _allCheckBox
                 .Where(checkBox => checkBox.Tag != null && checkBox.Tag.Equals("Size Ratio"))
                 .Where(checkBox => checkBox.Checked).Select(checkBox => int.Parse(checkBox.Text.Split(':')[0].Trim()));
            // 大小比 過濾
            filterAllNumber = filterAllNumber.Where(n =>
            {
                var @char = n.ToCharArray().Select(c => int.Parse(c.ToString())).ToList();
                var init = 0;
                @char.ForEach(b =>
                {
                    if (b >= 5)
                    {
                        init++;
                    }
                });

                return sizeRatio.Contains(init) ? true : false;
            });
            return filterAllNumber;
        }

        private static IEnumerable<string> 和值過濾(this IEnumerable<string> filterAllNumber)
        {
            //和值 Check Box
            var total = _allCheckBox
                 .Where(checkBox => checkBox.Tag != null && checkBox.Tag.Equals("Total"))
                 .Where(checkBox => checkBox.Checked).Select(checkBox => checkBox.Text);
            //和值 過濾
            filterAllNumber = filterAllNumber.Where(n =>
            {
                var sum = n.ToCharArray().Select(c => int.Parse(c.ToString())).Sum().ToString().PadLeft(2, '0');
                return total.Contains(sum) ? true : false;
            });
            return filterAllNumber;
        }

        private static IEnumerable<string> 百十個位數過濾()
        {
            //百位數 Check Box
            var tripleDigit = _allCheckBox
                .Where(checkBox => checkBox.Tag != null && checkBox.Tag.Equals("Triple Digit"))
                .Where(checkBox => checkBox.Checked).Select(checkBox => checkBox.Text);

            //十位數 Check Box
            var doubleDigit = _allCheckBox
                 .Where(checkBox => checkBox.Tag != null && checkBox.Tag.Equals("Double Digit"))
                 .Where(checkBox => checkBox.Checked).Select(checkBox => checkBox.Text);

            //個位數 Check Box
            var singleDigit = _allCheckBox
                 .Where(checkBox => checkBox.Tag != null && checkBox.Tag.Equals("Single Digit"))
                 .Where(checkBox => checkBox.Checked).Select(checkBox => checkBox.Text);
            //過濾 百、十、個 位數
            var filterAllNumber = AllNumber.Where(n =>
            {
                var @char = n.ToCharArray().Select(c => c.ToString());
                return (tripleDigit.Contains(@char.ElementAt(0))
                            && doubleDigit.Contains(@char.ElementAt(1))
                                && singleDigit.Contains(@char.ElementAt(2)))
                                    ? true : false;
            });
            return filterAllNumber;
        }
    }
}